package handler

import (
	"fmt"
	"log"
	"net/http"
	"websocker/pubsub"
	"websocker/util/uuid"

	"github.com/gorilla/websocket"
)

// 定制websocket
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// 发布订阅结构类型值声明
var ps = &pubsub.PubSub{}

// websocket处理程序
func WebsocketHandler(w http.ResponseWriter, r *http.Request) {
	// 是否允许请求源标头访问检测
	upgrader.CheckOrigin = func(r *http.Request) bool {
		return true
	}

	// HTTP服务器连接到WebSocket协议
	conn, err := upgrader.Upgrade(w, r, nil)

	// HTTP服务器连接到WebSocket协议, 错误检测
	if err != nil {
		log.Println(err)
		return
	}

	// 初始化客户端连接结构体类型值
	client := pubsub.Client{Id: uuid.AutoUUID(), Connection: conn}

	// 添加客户端连接到发布订阅结构体类型值
	ps.AddClient(client)

	// 用于表示新的客户端连接到websocket服务
	fmt.Printf("New Client is connected, clients total: %d\n", len(ps.Clients))

	// 迭代读取客户端消息
	for {
		// 读取websocket连接客户端发布消息
		messageType, p, err := conn.ReadMessage()

		// 读取websocket连接客户端发布消息, 错误检测
		if err != nil {
			// 输出到控制台
			log.Println("Something want wrong", err)

			// 此客户端是断开或错误连接，我们确实需要从pubsub中删除订阅和删除客户端
			ps.RemoveClient(client)

			// 输出当前websocket连接数量及订阅数量
			fmt.Printf("clients len(%d), subscription(%d)\n", len(ps.Clients), len(ps.Subscriptions))

			// 直接返回
			return
		}

		// 处理接收到的客户端消息
		ps.HandlerReceiveMessage(client, messageType, p)
	}
}
