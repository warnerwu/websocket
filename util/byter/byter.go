package byter

import "fmt"

func GenerateMessage(format string, data ...interface{}) []byte {
	return []byte(fmt.Sprintf(format, data...))
}
