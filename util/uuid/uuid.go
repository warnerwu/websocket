package uuid

import "github.com/satori/go.uuid"

// 生成UUID
func AutoUUID() string {
	return uuid.Must(uuid.NewV4()).String()
}
