package pubsub

import (
	"encoding/json"
	"fmt"
	"log"
	"websocker/util/byter"

	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
)

// 常量列表
const (
	Publish     = "publish"     // 发布
	Subscribe   = "subscribe"   // 订阅
	Unsubscribe = "unsubscribe" // 取消订阅
)

// 发布订阅结构体类型声明
type PubSub struct {
	Clients       []Client
	Subscriptions []Subscription
}

// 客户端连接结构体类型声明
type Client struct {
	Id         string
	Connection *websocket.Conn
}

// 消息结构体类型声明
type Message struct {
	Action  string          `json:"action"`
	Topic   string          `json:"topic"`
	Message json.RawMessage `json:"message"`
}

// 发布结构体类型声明
type Subscription struct {
	Topic  string
	Client *Client
}

// 添加客户端连接到发布订阅结构体类型值
func (ps *PubSub) AddClient(client Client) *PubSub {
	ps.Clients = append(ps.Clients, client)
	// fmt.Printf("adding new client to the list, uuid:%s, clients:%d\n", client.Id, len(sub.Clients))
	client.Connection.WriteMessage(1, []byte("Hello Client ID:"+client.Id))
	return ps
}

// 删除websocket客户端连接
func (ps *PubSub) RemoveClient(client Client) *PubSub {
	// 首先删除此客户机的所有订阅
	for index, subscription := range ps.Subscriptions {
		if subscription.Client.Id == client.Id {
			ps.Subscriptions = append(ps.Subscriptions[:index], ps.Subscriptions[index+1:]...)
		}
	}

	// 从列表中删除客户端
	for index, cli := range ps.Clients {
		if cli.Id == client.Id {
			ps.Clients = append(ps.Clients[:index], ps.Clients[index+1:]...)
		}
	}

	return ps
}

// 获取订阅消息
func (ps *PubSub) GetSubscriptions(client *Client, topic string) []Subscription {
	// 初始化订阅结构体类型值
	var subscriptionSli []Subscription

	// 迭代发布订阅结构体类型值属性订阅结构体类型值切片
	for _, subscription := range ps.Subscriptions {
		// 存在websocket客户端
		if client != nil {
			// 同一websocket客户端及主题相同
			if subscription.Client.Id == client.Id && subscription.Topic == topic {
				subscriptionSli = append(subscriptionSli, subscription)
			}
		} else {
			// 存在订阅相同主题消息
			if subscription.Topic == topic {
				subscriptionSli = append(subscriptionSli, subscription)
			}
		}
	}

	return subscriptionSli
}

// 发布消息
func (ps *PubSub) Publish(excludeClient *Client, topic string, messages []byte) {
	// 获取订阅消息
	subscriptions := ps.GetSubscriptions(nil, topic)

	// 迭代获取每一个订阅消息
	for _, subscription := range subscriptions {
		// 跳过给发布者发布消息
		if excludeClient.Id == subscription.Client.Id {
			continue
		}

		// 发送到websocket连接客户端消息
		err := subscription.Client.Send(messages)
		// 发送到websocket连接客户端消息, 错误检测
		if err != nil {
			log.Printf("Send message to client, error:%v", err)
		}
		fmt.Printf("发布消息到Client:%s, 发布消息内容为:%s\n", subscription.Client.Id, messages)
	}
}

// 发送消息到websocket连接客户端
func (c *Client) Send(messages []byte) error {
	return c.Connection.WriteMessage(1, messages)
}

// 订阅消息
func (ps *PubSub) Subscribe(client *Client, topic string) *PubSub {
	// 获取订阅消息
	subscription := ps.GetSubscriptions(client, topic)

	// 已存在订阅消息
	if len(subscription) > 0 {
		return ps
	}

	// 初始化发布结构体类型值
	newSubscription := Subscription{
		Topic:  topic,
		Client: client,
	}

	// 追加到发布订阅结构体类型值
	ps.Subscriptions = append(ps.Subscriptions, newSubscription)

	return ps
}

// 取消消息订阅
func (ps *PubSub) Unsubscribe(client *Client, topic string) (unsubscribe bool, err error) {
	// 迭代找到对应客户端websocket连接
	for index, subscription := range ps.Subscriptions {
		if subscription.Client.Id == client.Id && subscription.Topic == topic {
			ps.Subscriptions = append(ps.Subscriptions[:index], ps.Subscriptions[index+1:]...)
			return true, nil
		}
	}
	return false, errors.Errorf("Unsubscribed topic %s", topic)
}

// 处理接收到的客户端消息
func (ps *PubSub) HandlerReceiveMessage(client Client, messageType int, data []byte) *PubSub {
	// 初始化消息结构体类型值
	m := &Message{}

	// 通过JSON解码方式将数据赋值于消息结构体类型值
	err := json.Unmarshal(data, m)

	// 通过JSON解码方式将数据赋值于消息结构体类型值, 错误检测
	if err != nil {
		fmt.Println("This is not correct message data")
		return ps
	}

	// 输出有客户端消息
	fmt.Printf("Client correct message data: %+v\n", m)

	// 消息类型匹配
	switch m.Action {
	case Publish:
		// 发布消息
		ps.Publish(&client, m.Topic, m.Message)
	case Subscribe:
		// 订阅消息
		ps.Subscribe(&client, m.Topic)
		// 输出订阅消息结构体
		fmt.Printf("new subscriber to topic:%s, subscription len(%d), client id:%s\n", m.Topic, len(ps.Subscriptions), client.Id)
	case Unsubscribe:
		// 取消消息订阅
		unsubscribe, err := ps.Unsubscribe(&client, m.Topic)
		// 取消消息订阅, 错误检测
		if err != nil {
			fmt.Printf("The unsubscribe message failed, error:%v", err)
		} else {
			fmt.Printf("The unsubscribe message successful, status:%t, subscriptions len(%d)", unsubscribe, len(ps.Subscriptions))
		}
		// 发送消息到websocket连接客户端
		err = client.Send(byter.GenerateMessage("The unsubscribe message successful, topic:%s", m.Topic))
		// 发送消息到websocket连接客户端, 错误检测
		if err != nil {
			log.Printf("Seed message to client: %s, error:%v", client.Id, err)
		}
	}

	return ps
}
