package main

import (
	"log"
	"net/http"
	"websocker/router"
)

func main() {
	// 服务端口号
	endPoint := ":3000"

	// 设置路由
	router.Setup()

	// 当前服务运行于地址端口号
	log.Printf("Server is running: http://localhost%s\n", endPoint)

	// 运行服务
	err := http.ListenAndServe(endPoint, nil)

	// 运行服务, 错误检测
	if err != nil {
		panic(err)
	}
}
