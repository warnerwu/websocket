package router

import (
	"net/http"
	"websocker/handler"
)

func Setup() {
	// 根路径路由注册
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./static/index.html")
	})

	// websocket路由注册
	http.HandleFunc("/ws", handler.WebsocketHandler)
}
